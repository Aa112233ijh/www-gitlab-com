---
layout: job_family_page
title: "Support Operations Specialist"
description: Support Operations Specialists at GitLab support the daily operations and systems of the global support engineering team
---

## Support Operations Specialist

As a Support Operations Specialist, you will be responsible for supporting the
day-to-day operations and software systems used by GitLab’s global Technical
Support team (including, but not limited to: our Zendesk instance(s), GitLab.com
projects/integrations, and Calendly setup). You will be able to juggle a
diverse workload that includes everything from managing user provisioning, to
troubleshooting custom applications in support of a global organization. As we
rely on our systems to support and scale the organization, you will be the
backbone and the foundation to our success!

### Job Grade 

The Support Operations Specialist is a
[grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### What you can expect in a Support Operations Specialist role at GitLab:

As a Support Operations Specialist you will:

* Develop a flexible process and tool execution strategy to support the growth
  and scalability objectives of GitLab Support and company wide initiatives.
* Own and drive projects to completion in agreement with relevant stakeholders
  in a cross-departmental function.
* Manage a fast-paced queue of support operations requests, prioritize requests
  according to business impact, and drive to appropriate completion dates.
* Support and maintain several business critical SaaS systems (such as Zendesk,
  GitLab.com, Slack, Salesforce, Google groups), including user
  administration, in accordance with written and audited security controls and
  configuration changes when needed.
* Develop and maintain system failover processes for customer facing GitLab
  Support systems.
* Provide support and troubleshooting for our Support systems when necessary.
* Maintain and demonstrate 100% compliance with all written security policies
  and change management controls, as well as assisting with audits of user
  access to key systems.
* Understand internal customer's needs and business context to provide
  outstanding support and maintain high customer satisfaction.
* Assist with implementation of new systems as needed to support evolving
  processes and critically evaluate our use of systems with a view to improve
  global efficiency.

### Projects you might work on:

When you are not tackling your normal day to day challenges, you’ll have a lot
of freedom to work on things that will make your life, and the lives of your
coworkers, easier. Current and past Support Operations have:

* Created
  [tooling to generate weekly 1-1 issues](https://gitlab.com/gitlab-com/support/toolbox/1-1-issue-generator)
  for Support Engineers containing metrics, random tickets to review, etc.
* Developed
  [tooling to generate weekly account deletion reports](https://about.gitlab.com/handbook/support/support-ops/documentation/adwr.html).
* Leaned into
  [dogfooding](https://about.gitlab.com/handbook/engineering/principles/#dogfooding) by
  having various aspects of Zendesk
  [version controlled via GitLab.com](https://about.gitlab.com/handbook/support/support-ops/documentation/sync_repos.html).
* ...and even more!

### Requirements

#### Requires Skills/Experience:

* 1-3 years experience in SaaS support, with proven ability to support diverse
  customers needs.
* Zendesk Administrator experience (preferred; or the ability to gain Zendesk
  Administrator Certification within 6 months of start date if required)
* Demonstrated technical aptitude for, and experience supporting, client-server
  and/or web-based applications and SaaS/PaaS solutions
* Proven ability to solve practical business problems
* Understanding of business processes and ability to translate business
  requirements into application functionality
* Zendesk enthusiast who thrives on working in a fast-paced and changing
  environment
* Strong team player with service-oriented attitude and customer focus
* Excellent written and verbal communication
* An eye for detail and out-of-the-box thinking
* You share [our values](https://about.gitlab.com/handbook/values/), and work
  in accordance with those values
* Successful completion of a background check
* An ability to use GitLab, or a readiness to learn how to fully utilize GitLab

#### Desired Skills

* Demonstrated understanding of technical software support processes and
  concepts
* Experience in CRM or a related industry
* Familiarity with change management processes and risk control compliance
* Experience working on the Zendesk platform as a developer
* Experience with enterprise integration tools 
* Coding experience with an object oriented language (such as Ruby, Python,
  etc.)

### Senior Support Operations Specialist

A Senior Support Operations Specialist is a more experienced engineer who
continues to contribute as an individual while operating with equal comfort at
the level of team enablement. That is, they work at the team level to increase
every member's ability to contribute.

#### Job Grade

The Senior Support Engineer is a
[grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### What you can expect in a Senior Support Operations Specialist role at GitLab:

Generally they meet the following criteria. They:

* are involved in mentoring teammates on new technologies, new
  processes/workflows, and new GitLab features.
* posses expert knowledge of the various odds and ends of being a Support
  Operations specialist.
* submit merge requests to improve on Support Operations workflows/processes.
* drive system enhancements and fixes based on product expertise and GitLab
  Support interactions.
* contribute to one or more complementary projects.

A Senior Support Operations Specialist may be interested in exploring Support
Operations Management as an alternative at this point. See
[Engineering Career Development](https://about.gitlab.com/handbook/engineering/career-development/)
for more details.

### Career Ladder

For more details on the engineering career ladders, please review the
[engineering career development](https://about.gitlab.com/handbook/engineering/career-development/#roles)
handbook page.

### How you'll be measured

#### Performance Indicators

Support Operations collaborates with the global
[Support Engineering team](https://about.gitlab.com/support/) to achieve the
following performance indicators, by ensuring we have the most efficient
workflows through our applications:

* Providing top tier support to the GitLab Support Engineering team
* Driving the Support Operations
  [mission](https://about.gitlab.com/handbook/support/support-ops/#mission) and
  [vision](https://about.gitlab.com/handbook/support/support-ops/#vision)
* Maintenance of
  [Support Operations documentation](https://about.gitlab.com/handbook/support/support-ops/documentation/)
* Adherence and improvement of
  [Support Operations workflows](https://about.gitlab.com/handbook/support/support-ops/workflows/)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order
below. Please keep in mind that candidates can be declined from the position at
any stage of the process. To learn more about someone who may be conducting the
interview, find their job title on our
[team page](https://about.gitlab.com/company/team).

* Qualified candidates will be invited to schedule a 30 minute screening call
  with our Global Recruiters.
* Selected candidates receive a short assessment from our Global Recruiters.
* Candidates will then be invited to schedule a 30 minute technical interview
  with a Support Operations Specialist.
* Candidates will then be invited to schedule a 45 minute behavioral interview
  with a panel consisting of a Support Operations Manager and a Support
  Engineering Manager.
* Candidates will then be invited to schedule a 45 minute behavioral interview
  with our VP of Customer Support.
* Candidates will then undergo reference checks.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our
[hiring page](https://about.gitlab.com/handbook/hiring/).

### Compensation Calculator

To find out more about the compensation for this role, please apply to a open role on our [careers page](jobs/careers/) first. Once you've applied and are selected for a screening call, you'll be able to sign up here to view our
[compensation calculator](https://comp-calculator.gitlab.net/?role=Support%20Engineer).
Be sure to use the same email address for both.


### About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a
community project to which over 2,200 people worldwide have contributed. We are
an active participant in this community, trying to serve its needs and lead by
example. We have one [vision](https://about.gitlab.com/strategy): everyone can
contribute to all digital content, and our mission is to change all creative
work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency,
sharing, freedom, efficiency,
[self-learning](https://about.gitlab.com/company/culture/all-remote/self-service/#how-self-learning-leads-to-success-in-your-role),
frugality, collaboration, directness, kindness, diversity, inclusion and
belonging, boring solutions, and quirkiness. If these values match your
personality, work ethic, and personal goals, we encourage you to visit our
[primer](https://about.gitlab.com/company/) to learn more. Open source is our
culture, our way of life, our story, and what makes us truly unique.
