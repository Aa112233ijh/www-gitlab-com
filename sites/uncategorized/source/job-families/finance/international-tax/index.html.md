---
layout: job_family_page
title: "International Tax"
---

## International Tax Manager 

## Levels

### International Tax Manager (Intermediate)

The International Tax Manager (Intermediate) reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### International Tax Manager (Intermediate) Job Grade 

The International Tax Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### International Tax Manager (Intermediate) Responsibilities

* Play a significant role in establishing GitLab’s global tax strategy
* Tax Planning
  * Support the implementation of non-structural tax planning strategies
  * Participate in proposed Transactions to advise the Finance Team on Tax Consequences
  * Support USA and foreign tax integration of acquisitions, dispositions and restructurings into the corporate structure
* Transfer Pricing
  * Ensure adherence to the Transfer Pricing Concept
  * Maintain Transfer Pricing Documentation
* Tax Compliance
  * Ensure Global Corporate / State Income Tax Compliance
  * Closely work together with finance team members on VAT, Sales, Use Tax compliance
  * Ensure Global VAT compliance to electronic services
  * Take ownership of the control cycle to mitigate risks of Permanent Establishments
  * Take ownership of the cycle to mitigate employment tax risks
* Tax Audits
  * Evaluate requests and participate in developing strategies
  * Support the coordination and implementation of tax audit strategies
* Financial Reporting
  * Support preparation current / deferred income tax expense for financial statement purposes
  * Evaluate uncertain tax reporting positions and business combinations
  * Analyze the tax aspects of financial projections and forecasts
  * Lead technology and process improvements to ensure accurate, timely information is provided to the Finance team
  * Implementation and assurance of control cycle compliant with Sarbanes-Oxley
* Departmental liaison with IT staff on technical matters relating to tax applications

#### International Tax Manager (Intermediate) Requirements 

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* 5-8 years experience in a Big 4 environment and/or Business environment
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* Working knowledge of multiple International tax areas
* Ability to managing multiple projects at the same time
* Ability to use GitLab

### Senior International Tax Manager

The Senior International Tax Manager reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### Senior International Tax Manager Job Grade 

The Senior International Tax Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior International Tax Manager Responsibilities

* Extends that of the International Tax Manager (Intermediate) responsibilities

#### Senior International Tax Manager Requirements 

* Extends that of the nternational Tax Manager (Intermediate) requirements
* Degree in Business Taxation (Master's / JD / CPA)
* 8-12 years experience in a Big 4 environment and Business environment
* Successfully leading large projects and/or change management projects
* Strong influencing skills, high adaptability and analytical thinking
* Expert in multiple International tax areas
* Leading multiple projects at the same time

### Staff International Tax Manager

The Staff International Tax Manager reports to [Director of Tax](/job-families/finance/director-of-tax/).

#### Staff International Tax Manager Job Grade

The Staff International Tax Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff International Tax Manager Responsibilities

* Extends that of the Senior International Tax Manager responsibilities

#### Staff International Tax Manager Requirements

* Extends that of the Senior International Tax Manager requirements
* Degree in Business Taxation (Master's / JD / CPA)
* 15+ years experience in a Big 4 environment and Business environment
* Strong business judgment applied to tax and finance operational activities
* Proven international experience working with overseas operations
* Proven success with managing tax on a global scale within a fast pace environment
* Proven ability to drive change in tax processes with a technology driven mindset
* Demonstrated experience in mentoring and leading a distributed team
* Leadership experience in a software or global technology company
* Proven strategic and tactical vision to lead a high performing team

### Director, Tax

The Director, Tax reports to the VP, Tax.

### Director, Tax Job Grade 

The Director, Tax is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Director, Tax Responsibilities

* Primary responsibility for establishing GitLab’s international tax strategy.
* Responsible for completion of national and/local state income tax returns including all related analysis and support.
* Oversight of the tax returns for the Company’s in all operational jurisdictions.
* Responsible for VAT, Sales, Use and Property tax functions.
* Responsible for audits of international, federal and state income tax and state and local filings.
* Responsible for accounting for income taxes (ASC 740) in the US and International subsidiaries.
* Coordination of accounting for income taxes for international subsidiaries with third party vendors.
* Responsible for transfer pricing and management fee arrangements.
* Responsible for building and growing team of professional and paraprofessional staff.
* Liaison with operating management on tax issues and accounting staff on tax accounting issues.
* Ensuring that appropriate internal controls are in place over accounting for income taxes.
* Departmental liaison with IT staff on all technical matters relating to tax applications.
* Ensure the Company implements and maintains controls compliant with Sarbanes-Oxley.

### Director, Tax Requirements

* Bachelor’s Degree (B.S.) in Accounting. Master’s Degree in Business Taxation preferred.
* JD and/or CPA preferred.
* 10 years of related experience with at least 5 years in a public accounting firm.
* Experience with Software and/or SAAS in a high growth environment.
* Must have demonstrated a hands-on approach and success in working in a team-based environment
* International tax experience required with preference for candidates based in the Netherlands, UK or Ireland.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
* Ability to use GitLab

## Performance Indicators

* [Effective Tax Rate](https://about.gitlab.com/handbook/tax/performance-indicators/#effective-tax-rate-etr)
* [Budget vs. Actual](https://about.gitlab.com/handbook/tax/performance-indicators/#budget-vs-actual)
* [Audit Adjustments](https://about.gitlab.com/handbook/tax/performance-indicators/#audit-adjustments)

## Career Ladder

The next steps for the International Tax job family is not yet defined.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- 45 minute interview with the Director of Tax
- 30 minute interview with our International Tax Manager
- 30 minute interview with our Tax Accountant
- 30 minute interview with our Sr. Director, Controller

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
